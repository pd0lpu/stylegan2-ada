# StyleGAN2-ADA

This repository exists in order to make whole workflow of training a StyleGAN2 as easy as possible on [Paperspace](https://paperspace.com). Documentation coming soon.

## Acknowledgements

* [StyleGAN2-ADA](https://github.com/NVlabs/stylegan2-ada) by NVIDIA
* [CLIP](https://github.com/openai/CLIP) by OpenAI

Special thanks to Robert A. Gonsalves article found at [Towards Data Science](https://towardsdatascience.com/neural-kimono-or-intertextuality-of-ai-218fda33ab41). This repository borrows a lot of code from his article and would not exist without it.